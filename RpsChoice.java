//Shay Alex Lelichev 2043812
import javafx.event.*;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
    
    private TextField hi;
    private TextField wn;
    private TextField ls;
    private TextField tie;
    private String ch;
    private RpsGame game;

    public RpsChoice(TextField hi , TextField wn , TextField ls ,TextField tie , String ch , RpsGame game) {

        this.hi = hi;
        this.wn = wn;
        this.ls = ls;
        this.tie = tie;
        this.ch = ch;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e) {
        String chPc = game.playRound(ch);
        this.hi.setText(chPc);
        this.wn.setText("Wins: " + this.game.getWins());
        this.ls.setText("Losses: " + this.game.getLosses());
        this.tie.setText("Ties: " + this.game.getTies());
    }
}
