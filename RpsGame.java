//Shay Alex Lelichev 2043812
import java.util.Random;

public class RpsGame {

    private int wins;
    private int ties;
    private int losses;

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }

    public int getLosses() {
        return this.losses;
    }

    public String playRound(String choice) {

        Random rn = new Random();
        int randomNb = rn.nextInt(3);
        int pChoice = 0;
        String pcChoice = null;
        if (choice.equals("rock")) {
            pChoice = 0;
        } else if (choice.equals("scissors")) {
            pChoice = 1;
        } else if (choice.equals("paper")) {
            pChoice = 2;
        }

        if (randomNb == 0) {
            pcChoice = "rock";
        } else if (randomNb == 1) {
            pcChoice = "scissors";
        } else if (randomNb == 2) {
            pcChoice = "paper";
        }
        
        if (pChoice == randomNb) {
            this.ties++;
            return "Computer:  " + pcChoice + ". Tie.";
        } else if (randomNb == 0 && pChoice == 2 || randomNb == 1 && pChoice == 0 || randomNb == 2 && pChoice==1) {
            this.wins++;
            return "Computer: " + pcChoice + ". Player wins.";
        } else if (pChoice == 0 && randomNb == 2 || pChoice == 1 && randomNb == 0 || pChoice == 2 && randomNb == 1) {
            this.losses++;
            return "Computer:  " + pcChoice + ". Computer wins.";
        }

        return pcChoice;
    }
}