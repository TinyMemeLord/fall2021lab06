//Shay Alex Lelichev 2043812
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {	

    private RpsGame rpsGame;

	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene);
        
        Button btn1 = new Button("rock");
        Button btn2 = new Button("scissors");
        Button btn3 = new Button("paper");
        HBox buttons = new HBox(btn1 , btn2 , btn3);
        TextField messages = new TextField("Welcome!");
        messages.setPrefWidth(200);
        TextField wn = new TextField("Wins: 0");
        TextField ls = new TextField("Losses: 0");
        TextField tie = new TextField("Ties: 0");
        HBox textFields = new HBox(messages , wn , ls , tie);
        VBox vb = new VBox();
        vb.getChildren().addAll(buttons , textFields);
        root.getChildren().add(vb);
        RpsGame gm = new RpsGame();

        btn1.setOnAction(new RpsChoice(messages, wn, ls, tie, btn1.getText(), gm));
        btn2.setOnAction(new RpsChoice(messages, wn, ls, tie, btn2.getText(), gm));
        btn3.setOnAction(new RpsChoice(messages, wn, ls, tie, btn3.getText(), gm));

		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
